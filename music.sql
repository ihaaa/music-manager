-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 01, 2018 at 09:44 AM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `music`
--

-- --------------------------------------------------------

--
-- Table structure for table `albums`
--

DROP TABLE IF EXISTS `albums`;
CREATE TABLE IF NOT EXISTS `albums` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Album ID',
  `record_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Album Title',
  `performer` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Artist',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `albums`
--

INSERT INTO `albums` (`id`, `record_title`, `performer`) VALUES
(1, 'Sempiternal', 'Bring Me The Horizon'),
(3, 'Sprained Ankle', 'Julien Baker'),
(4, 'Thirteenth Step', 'A Perfect Circle'),
(5, '10,000 Days', 'Tool'),
(6, 'Threads', 'Now, Now'),
(13, 'test rand', 'tester');

-- --------------------------------------------------------

--
-- Table structure for table `songs`
--

DROP TABLE IF EXISTS `songs`;
CREATE TABLE IF NOT EXISTS `songs` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Song ID',
  `song_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Song Title',
  `album_id` int(11) NOT NULL COMMENT 'Album ID',
  PRIMARY KEY (`id`),
  KEY `album_id` (`album_id`)
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `songs`
--

INSERT INTO `songs` (`id`, `song_title`, `album_id`) VALUES
(1, 'Sleepwalking', 1),
(2, 'Shadow Moses', 1),
(4, 'Rejoice', 3),
(5, 'Pet', 4),
(6, 'The Pot', 5),
(7, 'Vicarious', 5),
(8, 'Jambi', 5),
(9, 'Lipan Conjuring', 5),
(10, 'Rosetta Stoned', 5),
(11, 'Threads', 6),
(12, 'Prehistoric', 6),
(96, 'test1', 13),
(97, 'test2', 13),
(98, 'test3', 13),
(99, 'test4', 13),
(100, 'test5', 13),
(102, 'test6', 13),
(103, 'test7', 13),
(104, 'test8', 13),
(105, 'test9', 13),
(106, 'test10', 13),
(107, 'test11', 13);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `songs`
--
ALTER TABLE `songs`
  ADD CONSTRAINT `songs_ibfk_1` FOREIGN KEY (`album_id`) REFERENCES `albums` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
