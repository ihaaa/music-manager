<?php

	include "classes.php";

	$Database = new Database();

	if (!empty($_GET['id'])) {

		$Database->queryS("SELECT * FROM albums WHERE id = :id");
		$Database->bindS(":id", $_GET["id"]);
		$albumsArray = $Database->fetchS();

	}

	if ($_SERVER["REQUEST_METHOD"] == "POST") {

		$Database->deleteAll();

	}

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

	<title>Delete Albums</title>

	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>

	<nav class="navbar navbar-default">
	  <div class="container container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="index.php">Music Manager</a>
	    </div>

	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav navbar-right">
	        <li><a href="index.php">Browse Albums</a></li>
	        <li><a href="add.php">Add Albums</a></li>
	        <li><a href="edit.php">Edit Albums</a></li>
	        <li class="active"><a href="delete.php">Delete Albums <span class="sr-only">(current)</span></a></li>
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>

	<div id="scrollingBanner">here goes the scrolling banner</div>

	<div class="container">

		<div class="horizon"><?php $Database->displayAllHorizontally(); ?></div>

		<div class="vertical"><?php $Database->displayAllVertically(); ?></div>

		<?php 
			if(!empty($_GET['id'])) {
				$Database->queryS("SELECT * FROM albums WHERE id = :id");
				$Database->bindS(":id", $_GET['id']);
				$albumTest = $Database->fetchS();

				if (empty($albumTest)) {
					echo "<div class='delete error center'>The record can not be deleted from the database.</div>";
				} else {
		?>

		<form name="thisForm" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
			<fieldset>

				<legend class="center">Delete Album</legend>
				
				<?php echo "<div class='error center'>Are you sure you want to DELETE album: <u>" . $albumsArray[0]['record_title'] . "</u> by: <u>" . $albumsArray[0]['performer'] . "</u></div>"; ?>

				<input type="hidden" name="id" value="<?php if(!empty($_GET['id'])) { echo $_GET['id']; } ?>" />

				<div class='delete center row'>
					<div class='col-md-12 col-xs-12'> 
			  		<input type="submit" name="submit" value="Delete" />
			  	</div>
			  </div>

			</fieldset>
		</form>

		<?php }} ?>

		<div id="bottom"></div>

	</div>

	<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <script type="text/javascript" src="js/mainAdd.js"></script>

</body>
</html>