<?php

	include "classes.php";
	include "filter.php";

	$Database = new Database();

	if (!empty($_GET['id'])) {

		$Database->queryS("SELECT song_title FROM songs WHERE album_id = :id");
		$Database->bindS(":id", $_GET["id"]);
		$songsArray = $Database->fetchS();

		$Database->queryS("SELECT * FROM albums WHERE id = :id");
		$Database->bindS(":id", $_GET["id"]);
		$albumsArray = $Database->fetchS();

	}

	if ($_SERVER["REQUEST_METHOD"] == "POST") {

		if (!$_POST['recordTitle']) {
			$errorT = "An album can not be saved without a title.";
		} 

		if (!$_POST['performer']) {
			$errorP = "An album can not be saved without a performer.";
		} 

		if (!$_POST['songs']) {
			$errorS = "An album can not be saved without at least one song.";
		}

		if (!empty($_POST['recordTitle']) && !empty($_POST['performer']) && !empty($_POST['songs'])) {
			$Database->updateAll();
		}

	}

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

	<title>Edit Albums</title>

	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>

	<nav class="navbar navbar-default">
	  <div class="container container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="index.php">Music Manager</a>
	    </div>

	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav navbar-right">
	        <li><a href="index.php">Browse Albums</a></li>
	        <li><a href="add.php">Add Albums</a></li>
	        <li class="active"><a href="edit.php">Edit Albums <span class="sr-only">(current)</span></a></li>
	        <li><a href="delete.php">Delete Albums</a></li>
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>

	<div id="scrollingBanner">here goes the scrolling banner</div>

	<div class="container">

		<div class="horizon"><?php $Database->displayAllHorizontally(); ?></div>

		<div class="vertical"><?php $Database->displayAllVertically(); ?></div>

		<div class='error center'><?php echo $errorT; ?></div>
		<div class='error center'><?php echo $errorP; ?></div>
		<div class="error center"><?php echo $errorS; ?></div>

		<?php 
			if(!empty($_GET['id'])) {
				$Database->queryS("SELECT * FROM albums WHERE id = :id");
				$Database->bindS(":id", $_GET['id']);
				$albumTest = $Database->fetchS();

				if (empty($albumTest)) {
					echo "<div class='error center'>The album specified can not be loaded for edit.</div>";
				} else {

		?>

		<form name="thisForm" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
			<fieldset>

				<legend class="center">Edit Album</legend>
				<div class='row'>
					<div class='right col-md-6 col-sm-6 col-xs-12'>
					  <label for="recordTitle">Record title:</label>
					</div>
					<div class='left col-md-6 col-sm-6 col-xs-12'>
					  <input id="recordTitle" type="text" name="recordTitle" value="<?php echo $albumsArray[0]['record_title']; ?>" />
					</div>
				</div>

				<div class='row'>
					<div class='right col-md-6 col-sm-6 col-xs-12'>	  
					  <label for="performer">Performer:</label>
					</div>
					<div class='left col-md-6 col-sm-6 col-xs-12'>
					  <input id="performer" type="text" name="performer" value="<?php echo $albumsArray[0]['performer']; ?>" />
					</div>
				</div>

				<div class='row'>
					<div class='right col-md-6 col-sm-6 col-xs-12'>	  
					  <label for="songs">Songs:</label>
					</div>
					<div class='left col-md-6 col-sm-6 col-xs-12'>
						<div class="songList">
							<div class="song">
						  	<input id="songs" type="text" name="songs[]" value="<?php echo $songsArray[0]['song_title']; ?>" />
						  </div>
						  <?php 
						  	for ($i=1; $i < count($songsArray); $i++) { 
						  		echo "<div class='song'><input type='text' name='songs[]' value='" . $songsArray[$i]['song_title'] . "' /></div>";
						  	}
						  ?>
						</div>
					  <a href="" class="addSong">Add More Songs</a>
					  <input type="hidden" name="id" value="<?php if(!empty($_GET['id'])) { echo $_GET['id']; } ?>" />
					</div>
				</div>

				<div class='center row'>
					<div class='col-md-12 col-xs-12'> 
			  		<input type="submit" name="submit" value="Edit" />
			  	</div>
			  </div>

			</fieldset>
		</form>

		<?php }} ?>

		<div id="bottom"></div>

	</div>

	<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <script type="text/javascript" src="js/mainAdd.js"></script>

</body>
</html>