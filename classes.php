<?php

//Database class
class Database {

	private $host = "127.0.0.1";
	private $db = "music";
	private $user = "root";
	private $pass = "";
	private $charset = "utf8mb4";

	private $pdo;
	private $stmt;

	public function __construct() {

		$dsn = "mysql:host=" . $this->host . ";dbname=" . $this->db . ";charset=" . $this->charset;

		$opt = array(
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
			PDO::ATTR_EMULATE_PREPARES => false,
		);

		$this->pdo = new PDO($dsn, $this->user, $this->pass, $opt);
	}

	public function queryS($query) {
		return $this->stmt = $this->pdo->prepare($query);
	}

	public function bindS($param, $value, $type = NULL) {
		if (is_null($type)) {
			switch (true) {
				case is_int($value):
					$type = PDO::PARAM_INT;
					break;

				case is_bool($value):
					$type = PDO::PARAM_BOOL;
					break;
					
				case is_null($value):
					$type = PDO::PARAM_NULL;
					break;

				default:
					$type = PDO::PARAM_STR;
					break;
			}
		}
		return $this->stmt->bindValue($param, $value, $type);
	}

	public function executeS() {
		return $this->stmt->execute();
	}

	public function fetchS() {
		$this->executeS();
		return $this->stmt->fetchAll();
	}

	// Various Queries

	public function displaySongs($id) {
		$this->queryS("SELECT song_title FROM songs WHERE album_id = $id ORDER BY id ASC");
		$songs = $this->fetchS();
		$songOL = "<ol>";
		foreach ($songs as $key) {
			foreach ($key as $value) {
				$songOL .= "<li>" . $value . "</li>";
			}
		}
		$songOL .= "</ol>";
		return $songOL;
	}

	public function displayAllHorizontally() {
		$this->queryS("SELECT * FROM albums ORDER BY record_title ASC");
		$albums = $this->fetchS();

		echo "<div class='row headerH'><div class='col-md-3 col-sm-3 col-xs-12'>Record title</div><div class='col-md-3 col-sm-3 col-xs-12'>Performer</div><div class='col-md-3 col-sm-3 col-xs-12'>Songs list</div><div class='col-md-3 col-sm-3 col-xs-12'>Operations</div></div>";
		
		foreach ($albums as $row) {
			echo "<div class='row ulAll'>";
			echo "<div class='col-md-3 col-sm-3 col-xs-12'>" . $row['record_title'] . "</div>";
			echo "<div class='col-md-3 col-sm-3 col-xs-12'>" . $row['performer'] . "</div>";
			echo "<div class='col-md-3 col-sm-3 col-xs-12'>" . $this->displaySongs($row['id']) . "</div>";
			echo "<div class='col-md-3 col-sm-3 col-xs-12'><a href='edit.php?id=" . $row['id'] . "#bottom'>&laquo;Edit&raquo;</a> <a href='delete.php?id=" . $row['id'] . "#bottom'>&laquo;Delete&raquo;</a></div>";
			echo "</div>";
		}
	}

	public function displayAllVertically() {
		$this->queryS("SELECT * FROM albums ORDER BY record_title ASC");
		$albums = $this->fetchS();
		
		foreach ($albums as $row) {
			echo "<div class='row ulAll'>";
			echo "<div class='col-md-6 col-sm-6 col-xs-6 headerV'>Record title:</div><div class='col-md-6 col-sm-6 col-xs-6'>" . $row['record_title'] . "</div>";
			echo "<div class='col-md-6 col-sm-6 col-xs-6 headerV'>Performer:</div><div class='col-md-6 col-sm-6 col-xs-6'>" . $row['performer'] . "</div>";
			echo "<div class='col-md-6 col-sm-6 col-xs-6 headerV'>Songs list:</div><div class='col-md-6 col-sm-6 col-xs-6'>" . $this->displaySongs($row['id']) . "</div>";
			echo "<div class='col-md-6 col-sm-6 col-xs-6 headerV'>Operations:</div><div class='col-md-6 col-sm-6 col-xs-6'><a href='edit.php?id=" . $row['id'] . "#bottom'>&laquo;Edit&raquo;</a> <a href='delete.php?id=" . $row['id'] . "#bottom'>&laquo;Delete&raquo;</a></div>";
			echo "</div>";
		}
	}

	public function insertAll() {
		$this->queryS("INSERT INTO albums (`id`, `record_title`, `performer`) VALUES (NULL, :record_title, :performer)");
		$this->bindS(":record_title", $_POST["recordTitle"]);
		$this->bindS(":performer", $_POST["performer"]);
		$this->executeS();

		$album_id = $this->pdo->lastInsertId();

		foreach ($_POST['songs'] as $value) {
			$this->queryS("INSERT INTO songs (`id`, `song_title`, `album_id`) VALUES (NULL, :song_title, :album_id)");
			$this->bindS(":song_title", $value);
			$this->bindS(":album_id", $album_id);
			$this->executeS();
		}

		echo "<div class='container'><div class='success center'>Record successfully added to the database.</div></div>"; //Should this be with try and catch?
	}

	public function deleteSong($id) {
		$this->queryS("DELETE FROM songs WHERE id = :id");
		$this->bindS(":id", $id);
		$this->executeS();
	}

	public function updateAll() {
	
		$this->queryS("UPDATE albums SET record_title = :record_title, performer = :performer WHERE id = :id");
		$this->bindS(":record_title", $_POST["recordTitle"]);
		$this->bindS(":performer", $_POST["performer"]);
		$this->bindS(":id", $_POST["id"]);
		$this->executeS();

		$this->queryS("SELECT id FROM songs WHERE album_id = :id ORDER BY id ASC");
		$this->bindS(":id", $_POST["id"]);
		$idOfSongs = $this->fetchS();
		//all song IDs from the album + the deleted ones

		$keys = array_keys($_POST['songs']);
		//keys of the songs that are left
		$numberOfSongs = end($keys) + 1;

		if ($numberOfSongs <= count($idOfSongs)) {
			for ($i=0; $i < count($idOfSongs); $i++) {
				$this->queryS("UPDATE songs SET song_title = :song_title WHERE id = :songId");
				if (isset($_POST['songs'][$i])) {
					$this->bindS(":song_title", $_POST['songs'][$i]);
					$this->bindS(":songId", $idOfSongs[$i]['id']);
					$this->executeS();
				} else {
					$this->deleteSong($idOfSongs[$i]['id']);
				}
			}
		} else {
			$editedSongs = 0;

			for ($i=0; $i < count($idOfSongs); $i++) {
				$this->queryS("UPDATE songs SET song_title = :song_title WHERE id = :songId");
				if (isset($_POST['songs'][$i])) {
					$this->bindS(":song_title", $_POST['songs'][$i]);
					$this->bindS(":songId", $idOfSongs[$i]['id']);
					$this->executeS();
					$editedSongs++;
				} else {
					$this->deleteSong($idOfSongs[$i]['id']);
				}
			}

			$leftovers = array_slice($_POST['songs'], $editedSongs);

			foreach ($leftovers as $value) {
				$this->queryS("INSERT INTO songs (`id`, `song_title`, `album_id`) VALUES (NULL, :song_title, :album_id)");
				$this->bindS(":song_title", $value);
				$this->bindS(":album_id", $_POST["id"]);
				$this->executeS();
			}
		}

		echo "<div class='container'><div class='success center'>Record successfully added to the database.</div></div>"; //Should this be with try and catch?

	}

	public function deleteAll() {
		$this->queryS("DELETE FROM albums WHERE id = :id");
		$this->bindS(":id", $_POST["id"]);
		$this->executeS();

		echo "<div class='container'><div class='success center'>The record was successfully deleted from the database.</div></div>"; //Should this be with try and catch?
	}


}