$(document).ready(function(){
  $('.addSong').on('click', function() {
    if ($('.songList .song').length <= 19) {
      $('.songList').append('<div class="song"><input type="text" name="songs[]"></div>');
    }

    $("html, body").animate({ scrollTop: $(document).height() }, 500);

    return false;
  });
});